import express from 'express';
const router = express.Router();

import { createProduct, deleteProduct, retrieveAllProducts, retrieveSingleProduct, updateProduct, retrieveAllOrders } from '../controllers/productsController.js';

router.route('/createProduct').post(createProduct);
router.route('/deleteProduct/:id').delete(deleteProduct);
router.route('/retrieveAllProducts').get(retrieveAllProducts);
router.route('/retrieveSingleProduct/:id').get(retrieveSingleProduct);
router.route('/updateProduct/:id').put(updateProduct);
router.route('/retrieveAllOrders').get(retrieveAllOrders);


export default router;

