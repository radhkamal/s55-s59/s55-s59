import express from 'express';
const router = express.Router();

import { register, login, updateUser, retrieveSingleOrder } from '../controllers/authController.js';

router.route('/register').post(register);
router.route('/login').post(login);
router.route('/updateUser').put(updateUser);
router.route('/retrieveSingleOrder/:id').get(retrieveSingleOrder);

export default router;