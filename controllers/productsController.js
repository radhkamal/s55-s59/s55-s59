
// admin only
const createProduct = async (req, res) => {
    res.send('create product');
}

// admin only
const deleteProduct = async (req, res) => {
    res.send('delete product');
}

// admin only
const retrieveAllProducts = async (req, res) => {
    res.send('retrieve all products');
}

const retrieveSingleProduct = async (req, res) => {
    res.send('retrieve single product');
}

// admin only
const updateProduct = async (req, res) => {
    res.send('update product');
}

// admin only
const retrieveAllOrders = async (req, res) => {
    res.send('retrieve all orders')
}


export { createProduct, deleteProduct, retrieveAllProducts, retrieveSingleProduct, updateProduct, retrieveAllOrders };