import express from 'express';
const app = express();

import dotenv from 'dotenv';
dotenv.config();

// middleware importation
import notFoundMiddleware from './middleware/notFound.js';
import errorHandlerMiddleware from './middleware/errorHandler.js';

// db and authenticateUser
import connectDB from './database/connect.js';

// routers
import authRouter from './routes/authRoutes.js';
import productRouter from './routes/productRoutes.js';

// JSON middleware
app.use(express.json());

app.get('/', (req, res) => {

    throw new Error('error');
    res.send('Welcome to FraCodeCcino Server!');
})

app.use('/users', authRouter);
app.use('/products', productRouter);

app.use(notFoundMiddleware);
app.use(errorHandlerMiddleware);

const port = process.env.PORT || 5000;

const start = async () => {
    try {
        
        await connectDB(process.env.MONGO_URL)
        app.listen(port, () => {
            console.log(`Server is running at port ${port}...`);
        })

    } catch (error) {
        console.log(error);
    }
};

start();