import React from 'react'

export const FormRow = ({type, name, value, handleChange, labelText}) => {

  return (
    <React.Fragment>
        <div className='form-row'>
            <label htmlFor={name} className='form-label'>
                {labelText || name}
            </label>
            <input
                type={type}
                value={value}
                name={name}
                onChange={handleChange}
                className='form-input'
            ></input>
        </div>
    </React.Fragment>
  )
}

export default FormRow;