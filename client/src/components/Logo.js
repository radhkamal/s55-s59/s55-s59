import logo from '../assets/images/fracc.svg';

const Logo = () => {
    return (
        <img src={logo} alt='jobify' className='logo'></img>
    );
};

export default Logo;