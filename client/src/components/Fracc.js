import fracc from '../assets/images/fracc.svg';

const Fracc = () => {
    return (
        <img src={fracc} className='logo'></img>
    );
};

export default Fracc;