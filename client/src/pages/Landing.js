import React from 'react';
import logo from '../assets/images/rad.svg';
import main from '../assets/images/frappe.png';
import styled from 'styled-components';

import '../index.css';

import Wrapper from '../assets/wrappers/Testing';
import { Logo } from '../components/index';
import { Link } from 'react-router-dom';

function Landing () {
  return (
    <Wrapper>
        <nav>
            <Logo/>
        </nav>
        
        <div className='container page'>
            {/* info */}
            <div className='info'>
                <h1>
                     Turning Sips to <span>Codes</span>
                </h1>
                <p>
                    One-stop glucose, fructose, and lactose shop for sugary developers. Whether it be pastries, coffees, fruit teas, or milk teas that you're leaning towards on, FraCodeCcino shall aid you with turning saccharides into codes since 199x.
                </p>
                <Link to='/register' className='btn btn-hero'>
                    Login/Register
                </Link>
            </div>
            <img src={main} alt='job hunt' className='main-img'></img>
        </div>
    </Wrapper>
  )
}

export default Landing;