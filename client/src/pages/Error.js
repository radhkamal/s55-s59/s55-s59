import React from 'react';
import { Link } from 'react-router-dom';
import img from '../assets/images/error.png';
import Wrapper from '../assets/wrappers/ErrorPage';

const Error = () => {
  return (
    <Wrapper className='full-page'>
        <div>
            <img src={img} alt='error'></img>
            <Link to='/'>Click to go Back Home</Link>
        </div>
    </Wrapper>
  )
}

export default Error;