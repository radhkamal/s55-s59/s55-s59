import React, { useReducer, useContext } from 'react';
import reducer from './reducer';

import { DISPLAY_ALERT, CLEAR_ALERT } from './actions';

// initial state for UseReducer
const initialState = {
    isLoading: false,
    showAlert: false,
    alertText: '',
    alertType: ''
}

const AppContext = React.createContext();

// provider for App Context
const AppProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const displayAlert = () => {
        dispatch({ type: DISPLAY_ALERT });
        clearAlert();
    }

    const clearAlert = () => {
        setTimeout(() => {
            dispatch({ type: CLEAR_ALERT})
        }, 3000);
    }

    return (
        <AppContext.Provider
            value={{ ...state, displayAlert }}
        >
            {children}
        </AppContext.Provider>
    );
}

// customHooks
const useAppContext = () => {
    return useContext(AppContext);
}

export { AppProvider, initialState, useAppContext };